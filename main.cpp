#include <iostream>
#include "threads.h"

using std::vector;

int main()
{
	std::vector<int> primes;
	primes = callGetPrimes(0, 1000);
	primes.clear();
	primes = callGetPrimes(0, 100000);
	primes.clear();
	primes = callGetPrimes(0, 1000000);


	callWritePrimesMultipleThreads(0, 1000, "tester.txt", 100);
	callWritePrimesMultipleThreads(0, 100000, "tester.txt", 100);
	callWritePrimesMultipleThreads(0, 1000000, "tester.txt", 100);

	bonus_3(1);
	bonus_1();

    return 0;
}
