//
//  threads.cpp
//  Tread
//
//  Created by daniel benassaya on 12/01/2020.
//  Copyright © 2020 daniel benassaya. All rights reserved.
//
#include "threads.h"
#include <math.h>


void I_Love_Threads()
{
    std::cout << "Threads Love I\n";
}

void call_I_Love_Threads()
{
    std::thread t1(I_Love_Threads);
    t1.join();
}

void printVector(std::vector<int> primes)
{
    int x, size = primes.size();
    for (int i = 0; i < size; i++)
    {
        x = primes.back();
        primes.pop_back();
        std::cout << x << std::endl;
    }
}

std::vector<int> callGetPrimes(int begin, int end)
{
    std::vector<int> primes;
	auto start = std::chrono::system_clock::now();
	std::thread t2(getPrimes, begin, end, std::ref(primes));
	t2.join();
	auto endd = std::chrono::system_clock::now();

	std::chrono::duration<double> elapsed_seconds = endd - start;
    
	std::cout << "time: " << elapsed_seconds.count() << std::endl;
    return primes;
}

void getPrimes(int begin, int end, std::vector<int>& primes)
{
    int flag;
    while (begin < end)
    {
        flag = 0;
        for(int i = 2, j = sqrt(begin); i <= j; i++)
        {
            if(begin % i == 0)
            {
                flag = 1;
                break;
            }
        }
        if (begin && flag == 0 && begin != 1)
        {
            primes.push_back(begin);
        }
        begin++;
    }
}

void writePrimesToFile(int begin, int end, std::ofstream& file)
{
	int flag;
	while (begin < end)
	{
		flag = 0;
		for (int i = 2, j = sqrt(begin); i <= j; i++)
		{
			if (begin % i == 0)
			{
				flag = 1;
				break;
			}
		}
		if (begin && flag == 0 && begin != 1)
		{
			file << begin << "\n";
		}
		begin++;
	}
}

void callWritePrimesMultipleThreads(int begin, int end, std::string filePath, int N)
{
	std::ofstream file(filePath);
	std::function<void(int begin, int end, std::ofstream & file)> f;

	if (N < 1 || begin < 0 || end < 2)
	{
		return;
	}
	auto start = std::chrono::system_clock::now();// start the stoper
	for (int i = 0, begin2 = 0, end2 = (end / N); i < N; i++, begin2 += (end / N), end2 += (end / N))
	{
		f = writePrimesToFile;
		f(begin2, end2, file);
		std::thread(f);
	}
	auto endd = std::chrono::system_clock::now();// stoping the stoper
	std::chrono::duration<double> elapsed_seconds = endd - start;
	std::cout << "time: " << elapsed_seconds.count() << std::endl;
}

void printColor(int num)
{
	setColor(num);
	std::cout << "color\n";
}

void setColor(int value)
{
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), value);
}

void bonus_1()
{
	std::function<void(int num)> f;
	for (int i = 0; i < 15; i++)
	{
		f = printColor;
		f(i);
		std::thread(f);
	}
}

void bonus_3(int num)
{
	if (num <= 50)
	{
		std::cout << "Hello frome Thread " << num << std::endl;
		std::thread t(bonus_3, (num + 1));
		t.join();
	}
}



