#pragma once

#include <Windows.h>
#include <string>
#include <fstream>
#include <iostream>
#include <vector>
#include <thread>
#include <chrono>
#include <functional>

// print "I_Love_Threads"
void I_Love_Threads();
// call I_Love_Threads func in thread
void call_I_Love_Threads();
// print a vector
void printVector(std::vector<int> primes);
//insert the prime number of the range begin - end in the vector
void getPrimes(int begin, int end, std::vector<int>& primes);
// call the getPrime func in thread
std::vector<int> callGetPrimes(int begin, int end);

// write prime nymber of range begin - end to the file
void writePrimesToFile(int begin, int end, std::ofstream& file);
// displaye the range to n range and make n thread of  writePrimesToFile func
void callWritePrimesMultipleThreads(int begin, int end, std::string filePath, int N);

//bonus
void printColor(int num);
void setColor(int value);
void bonus_1();
void bonus_3(int num);
